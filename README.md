# lv-novel

#### 介绍

lv-novel 是 基于 laravel、Dcat Admin、laravel-admin的基础上开发的小说CMS系统

#### 软件架构

php(putenv、proc_open不能禁用)、nginx、swoole


#### 安装教程

```$xslt
git clone https://gitee.com/wiera/lv-novel.git ./
cp .env.example .env
vi .env
// 修改里面的数据库信息为你新创建的数据库连接信息
php artisan key:generate
php artisan admin:install
// 这个命令会有一个"app/Admin directory already exists !"的提醒，这是因为我把我这边的安装文件提交到git了，忽略即可。
//为了保险，可以再执行下数据库迁移命令，如下
php artisan migrate
//安装passport
php artisan passport:install --uuids
php artisan passport:keys

chmod -R 777 storage bootstrap
#如果是nginx把属组给到www
chown www:www ./*

//以后升级系统的操作(更新代码，执行迁移)
git pull
php artisan migrate

```

#### 使用说明

> 暂无

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 鸣谢

`lv-novel` 基于以下组件:

+ [Laravel](https://laravel.com/)
+ [Dcat Admin](http://www.dcatadmin.com/)
+ [Laravel Admin](https://www.laravel-admin.org/)
+ [AdminLTE3](https://github.com/ColorlibHQ/AdminLTE)
+ [bootstrap4](https://getbootstrap.com/)
+ [jQuery3](https://jquery.com/)
+ [Eonasdan Datetimepicker](https://github.com/Eonasdan/bootstrap-datetimepicker/)
+ [font-awesome](http://fontawesome.io)
+ [jquery-form](https://github.com/jquery-form/form)
+ [moment](http://momentjs.com/)
+ [webuploader](http://fex.baidu.com/webuploader/)
+ [jquery-pjax](https://github.com/defunkt/jquery-pjax)
+ [Nestable](http://dbushell.github.io/Nestable/)
+ [toastr](http://codeseven.github.io/toastr/)
+ [editor-md](https://github.com/pandao/editor.md)
+ [fontawesome-iconpicker](https://github.com/itsjavi/fontawesome-iconpicker)
+ [layer弹出层](http://layer.layui.com/)
+ [char.js](https://www.chartjs.org)
+ [nprogress](https://ricostacruz.com/nprogress/)
+ [bootstrap-validator](https://github.com/1000hz/bootstrap-validator)
+ [Google map](https://www.google.com/maps)
+ [Tencent map](http://lbs.qq.com/)

## License
------------
`lv-novel` is licensed under [The MIT License (MIT)](LICENSE).


