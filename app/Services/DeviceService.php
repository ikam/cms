<?php

namespace App\Services;

use App\Models\Device;
use App\Utils\ResponseUtil;

class DeviceService extends BaseService
{
    function __construct() {

    }

    /**
     * 获取/存储设备信息
     *
     * @param array $udid
     * @return array
     */
    public function store($udid)
    {
        $dInfo['udid'] = $udid;
        $device = \Cache::get('d_'.$udid);
        if(is_null($device)){
            $device = Device::where($dInfo)->first();
            if (is_null($device))
                $device = Device::create($dInfo);
            \Cache::put('d_'.$udid, $device);
        }
        return $device;
        //return ResponseUtil::makeResponse('ok',$device);
    }

}
