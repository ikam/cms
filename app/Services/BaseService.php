<?php

namespace App\Services;

class BaseService
{
    protected static $service = [];

    /**
     * @Method  init
     *
     * @static
     * @return static
     */
    public static function init() {
        if (isset(self::$service[static::class])) {
            return self::$service[static::class];
        } else {
            $obj = new static();
            self::$service[static::class] = $obj;
            return $obj;
        }
    }

}

