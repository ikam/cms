<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        // Passport 路由
        Passport::routes();
        // Passport Token 过期时间
        Passport::tokensExpireIn(Carbon::now()->addDay(15));
        // Passport Refresh Token 过期时间
        Passport::refreshTokensExpireIn(Carbon::now()->addDay(30));
        // 个人token过期时间
        Passport::personalAccessTokensExpireIn(now()->addMonths(1));
        //Passport::personalAccessTokensExpireIn(now()->addMinutes(120));

        Passport::loadKeysFrom(storage_path('/'));
    }
}
