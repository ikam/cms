<?php

namespace App\Utils;

class ResponseUtil
{
    /**
 * @param string $message
 * @param mixed  $data
 * @param array  $append
 * @return array
 */
    public static function makeResponse($message, $data, $append = [])
    {
        if(!is_array($append)){
            $append = ['code'=>$append];
        }
        return [
                'success' => true,
                'data'    => $data,
                'message' => $message,
            ] + $append;
    }

    /**
     * @param string $message
     * @param array  $append
     *
     * @return array
     */
    public static function makeError($message, $append = [])
    {
        if(!is_array($append)){
            $append = ['code'=>$append];
        }
        return [
                'success' => false,
                'message' => $message,
            ] + $append;
    }
}
