<?php

namespace App\Admin\Repositories;

use App\Models\Device as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Device extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
