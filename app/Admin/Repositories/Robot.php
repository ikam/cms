<?php

namespace App\Admin\Repositories;

use App\Models\Robot as Model;
use Dcat\Admin\Repositories\EloquentRepository;

class Robot extends EloquentRepository
{
    /**
     * Model.
     *
     * @var string
     */
    protected $eloquentClass = Model::class;
}
