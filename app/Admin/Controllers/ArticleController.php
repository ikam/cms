<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Article;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class ArticleController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Article(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('user_id');
            $grid->column('title');
            //$grid->column('abstract');
            $grid->column('tags');
            //$grid->column('content');
            $grid->column('path');
            $grid->column('view_count');
            $grid->column('comment_count');
            $grid->column('status');
            $grid->column('ip');
            //$grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Article(), function (Show $show) {
            $show->field('id');
            $show->field('user_id');
            $show->field('title');
            $show->field('abstract');
            $show->field('tags');
            $show->field('content');
            $show->field('path');
            $show->field('view_count');
            $show->field('comment_count');
            $show->field('status');
            $show->field('ip');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Article(), function (Form $form) {
            $form->display('id');
            $form->text('user_id');
            $form->text('title');
            $form->text('abstract');
            $form->tags('tags');
            $form->text('content');
            $form->text('path');
            $form->text('view_count');
            $form->text('comment_count');
            $form->text('status');
            $form->text('ip');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
