<?php

namespace App\Admin\Controllers;

use App\Admin\Repositories\Device;
use Dcat\Admin\Form;
use Dcat\Admin\Grid;
use Dcat\Admin\Show;
use Dcat\Admin\Http\Controllers\AdminController;

class DeviceController extends AdminController
{
    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Grid::make(new Device(), function (Grid $grid) {
            $grid->column('id')->sortable();
            $grid->column('ver');
            $grid->column('udid');
            $grid->column('created_at');
            $grid->column('updated_at')->sortable();
        
            $grid->filter(function (Grid\Filter $filter) {
                $filter->equal('id');
        
            });
        });
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     *
     * @return Show
     */
    protected function detail($id)
    {
        return Show::make($id, new Device(), function (Show $show) {
            $show->field('id');
            $show->field('ver');
            $show->field('udid');
            $show->field('created_at');
            $show->field('updated_at');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Form::make(new Device(), function (Form $form) {
            $form->display('id');
            $form->text('ver');
            $form->text('udid');
        
            $form->display('created_at');
            $form->display('updated_at');
        });
    }
}
