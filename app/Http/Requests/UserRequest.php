<?php

namespace App\Http\Requests;

use App\Models\User;

class UserRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return User::RULES;
    }
}
