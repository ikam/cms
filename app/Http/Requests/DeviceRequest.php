<?php

namespace App\Http\Requests;

use App\Models\Device;

class DeviceRequest extends Request
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return Device::RULES;
    }
}
