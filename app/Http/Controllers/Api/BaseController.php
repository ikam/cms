<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Response;
use App\Utils\ResponseUtil;

class BaseController extends Controller
{

    /**
     * @param mixed  $data
     * @param string $message
     * @param array  $append
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse($data, $message, $append = [])
    {
        if(!is_array($append)){
            $append = ['code'=>$append];
        }
        return Response::json(ResponseUtil::makeResponse($message, $data, $append));
    }

    /**
     * @param string $error
     * @param mixed  $append 若为错误码，则直接是int数字
     * @param int  $code
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError($error, $append = [], $code = 200)
    {
        if(!is_array($append)){
            $append = ['code'=>$append];
        }
        return Response::json(ResponseUtil::makeError($error,$append), $code);
    }
}
