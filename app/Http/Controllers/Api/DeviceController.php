<?php

namespace App\Http\Controllers\Api;

use App\Services\DeviceService;
use Validator;
use App\Models\Device;
use Illuminate\Http\Request;

class DeviceController extends BaseController
{

    /**
     * 通用-创建|获取设备信息
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request)
    {
        $header['udid'] = $request->header('Udid');
        $validator = Validator::make($header, Device::RULES);
        if ($validator->fails()) {
            return $this->sendError($validator->errors());
        }
        $device = DeviceService::init()->store($header['udid'])->toArray();
        $device['server_time'] = time();
        $device['ip'] = $request->getClientIp();
        return $this->sendResponse($device,'ok');
    }
}
