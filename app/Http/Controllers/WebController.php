<?php
/*
 * 英文名:iKam-blog
 * 中文名:可爱猫博客
 * 一个极简博客系统，单类实现，理论上你把这个文件以及涉及到的数据库\公共方法\模版文件拷贝到你的laravel项目中，就能运行。
 * 当然，不建议你这么做，当前laravel也是极简功能，你完全可以在这基础上开发其他更多功能。
 * 官网:https://www.ikam.cn
 */

namespace App\Http\Controllers;

use Response;
use App\Utils\ResponseUtil;
use App\Models\Article;

class WebController extends Controller
{
    const IKAM_VER = '1.0.0';//博客版本号

    /**
     * 首页
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function home(){
        $articles = Article::where('status',1)
            ->with('author')
            ->orderBy('created_at', 'desc')
            ->paginate(2);

        $head = [
            'title'       => 1,
            'keywords'    => 2,
            'description' => 3,
        ];

        $assign = [
            'articles'     => $articles,
            'head'         => $head,
        ];
        //dd($articles);
        return view('index', $assign);
    }

    /**
     * 文章内容
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function content(){
        echo "home";
    }

    /**
     * 我的文章
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(){
        echo "index";
    }

    /**
     * show
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id){
        echo "show->{$id}";
    }

}
