<?php

namespace App\Models;

use Dcat\Admin\Traits\HasDateTimeFormatter;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use HasDateTimeFormatter;

    /**
     * 保存
     *
     * @var array
     */
    protected $fillable = [
        'udid',
    ];

    /**
     * 展示
     *
     * @var array
     */
    protected $visible = [
        'id','udid', 'created_at'
    ];

    /**
     * 隐藏
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * 强制类型转换.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * 后加属性
     *
     * @var array
     */
    protected $appends = [];
    const RULES = [
        'udid'    => 'required',
        //'ver'    => 'required',
    ];

    public function setUpdatedAt($value) {
        // Do nothing.
    }
}
