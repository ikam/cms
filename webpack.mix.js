const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset 管理
 |--------------------------------------------------------------------------
 |
 | Mix提供了一个干净，流畅的API，用于为您的Laravel应用程序定义一些Webpack构建步骤。
 | 默认情况下，我们正在为应用程序编译CSS文件，并捆绑所有JS文件。
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .vue()
    .extract(['vue'])
    .sass('resources/sass/app.scss', 'public/css')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);
