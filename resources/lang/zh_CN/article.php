<?php 
return [
    'labels' => [
        'Article' => 'Article',
        'article' => 'Article',
    ],
    'fields' => [
        'user_id' => '用户ID',
        'title' => '标题',
        'abstract' => '摘要',
        'tags' => '标签',
        'content' => '内容',
        'path' => '路径',
        'view_count' => '查看数',
        'comment_count' => '评论数',
        'status' => '状态',
        'ip' => 'IP',
    ],
    'options' => [
    ],
];
