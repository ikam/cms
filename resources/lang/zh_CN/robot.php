<?php 
return [
    'labels' => [
        'Robot' => '机器人列表',
        'robot' => '机器人列表',
    ],
    'fields' => [
        'user_id' => '管理员id',
        'type' => '机器人分类',
        'robot_id' => '机器人id',
        'config' => '机器人配置',
    ],
    'options' => [
    ],
];
