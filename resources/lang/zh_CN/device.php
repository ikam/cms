<?php 
return [
    'labels' => [
        'Device' => '设备列表',
        'device' => '设备列表',
    ],
    'fields' => [
        'ver' => '版本号',
        'udid' => 'UDID',
    ],
    'options' => [
    ],
];
