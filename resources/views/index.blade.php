@extends('layouts.app')
@section('title', $head['title'])
@section('keywords', $head['keywords'])
@section('description', $head['description'])
@section('content')

    <!-- 左侧列表开始 -->
    <div class="info-box">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item active">首页</li>
                {{--<li class="breadcrumb-item"><a href="#">Library</a></li>--}}
                {{--<li class="breadcrumb-item active" aria-current="page">Data</li>--}}
            </ol>
        </nav>
    </div>
    <div class="row">
        <!-- 循环文章列表开始 -->
        @foreach($articles as $k => $v)
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12 thumbnail">
                    {{--<div class="card bg-dark">--}}
                        {{--<a href="/{{ $v->id }}">--}}
                            {{--<img src="{{$v->images[0]['image']}}" class="card-img" alt="{{$v->images[0]['alt']}}">--}}
                        {{--</a>--}}
                            {{--<div class="card-img-overlay">--}}
                                {{--<a href="/{{ $v->id }}"><h5 class="card-title">{{ $v->title }}</h5></a>--}}
                                {{--<p class="card-text">{{ $v->abstract }}</p>--}}
                                {{--<p class="card-text">{{ $v->created_at }}</p>--}}
                                {{--@foreach($v->tags as $n)--}}
                                    {{--<a href="#" class="btn btn-primary">{{ $n }}</a>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                    {{--</div>--}}

                    <div class="card mb-3">
                        <h3 style="margin: 0.5rem 0 0 0.5rem;">{{ $v->title }}</h3>
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img class="col-xs-12 col-md-12 col-lg-12 card-body" src="{{$v->images[0]['image']}}" alt="{{$v->images[0]['alt']}}">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">{{ $v->title }}</h5>
                                    <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                    <p class="card-text"><small class="text-muted">{{ $v->created_at }}</small></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                {{--<div class="col-xs-12 col-md-12 col-lg-12 thumbnail">--}}
                    {{--<a href="/{{ $v->id }}">--}}
                        {{--<img src="{{$v->images[0]['image']}}" class="col-xs-12 col-md-12 col-lg-12 img-thumbnail img-rounded" alt="{{$v->images[0]['alt']}}">--}}
                    {{--</a>--}}
                {{--</div>--}}
                {{--<div class="col-xs-12 col-md-12 col-lg-12">--}}
                    {{--<i class="fa fa-user"></i> {{ $v->author->name }}--}}
                    {{--<i class="fa fa-calendar"></i> {{ $v->created_at }}--}}
                    {{--<i class="fa fa-list-alt"></i> <a href="#" target="_self">未分类</a>--}}
                    {{--@foreach($v->tags as $n)--}}
                        {{--<a class="b-tag-name" href="tags/1" target="_self">{{ $n }}</a>--}}
                    {{--@endforeach--}}
                    {{--{{ $v->abstract }}--}}
                {{--</div>--}}
            </div>
        </div>
        @endforeach
        <!-- 循环文章列表结束 -->
        <!-- 列表分页开始 -->
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
            {{ $articles->onEachSide(5)->links() }}
        </div>
        <!-- 列表分页结束 -->
    </div>
    <!-- 左侧列表结束 -->
@endsection

@section('right')
    <!-- 右侧列表开始 -->
    <form class="form-inline text-center" role="form">
        <div class="input-group mb-3">
            <input type="text" class="form-control" placeholder="请输入关键词" aria-label="请输入关键词" aria-describedby="button-addon">
            <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="button" id="button-addon">Go!</button>
            </div>
        </div>
    </form>

    <div class="card bg-dark text-white">
        <img src="/images/default.jpg" class="card-img" alt="内容以坐火箭飞走了">
        <div class="card-img-overlay">
            <h5 class="card-title">Card title</h5>
            <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
            <p class="card-text">Last updated 3 mins ago</p>
        </div>
    </div>
@endsection
