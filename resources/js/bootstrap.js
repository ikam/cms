window._ = require('lodash');

/**
 * 我们将加载jQuery和Bootstrap jQuery插件，
 * 该插件提供对基于JavaScript的Bootstrap功能（如模式和选项卡）的支持。
 * 可以修改此代码以适合您的应用程序的特定需求。
 */

try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

/**
 * 我们将加载axios HTTP库，该库使我们能够轻松地向Laravel后端发出请求。
 * 该库根据“ XSRF”令牌cookie的值自动处理将CSRF令牌作为标头发送。
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * 接下来，我们将CSRF令牌注册为Axios的通用标头，以便所有传出的HTTP请求都自动附加它。
 * 这只是一个简单的方便，因此我们不必手动附加每个令牌。
 */

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

/**
 * Echo公开了一个表达性的API，用于订阅频道并收听Laravel广播的事件。
 * Echo和事件广播使您的团队可以轻松构建强大的实时Web应用程序。
 */

// import Echo from 'laravel-echo';
//
// window.io = require('socket.io-client');
// window.Echo = new Echo({
//     broadcaster: 'socket.io',
//     host: window.location.hostname + ':6001'
// });

// Laravel官方推荐使用Pusher
// window.Pusher = require('pusher-js');
//
// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });
