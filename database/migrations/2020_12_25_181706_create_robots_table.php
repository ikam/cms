<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRobotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('robots', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->default('0')->nullable()->comment('管理员id');
            $table->string('type')->default('wx')->nullable()->comment('机器人分类');
            $table->string('robot_id',64)->nullable()->comment('机器人id');
            $table->string('config',2048)->nullable()->comment('机器人配置');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('robots');
    }
}
