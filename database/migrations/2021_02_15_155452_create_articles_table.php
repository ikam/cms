<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable()->comment('用户ID');
            $table->string('title',128)->nullable()->comment('标题');
            $table->string('abstract',512)->nullable()->comment('摘要');
            $table->string('tags',128)->nullable()->comment('标签');
            $table->text('content')->nullable()->comment('内容');
            $table->string('path',64)->nullable()->comment('路径');
            $table->integer('view_count')->default('0')->nullable()->comment('查看数');
            $table->integer('comment_count')->default('0')->nullable()->comment('评论数');
            $table->integer('status')->default('1')->nullable()->comment('状态');
            $table->string('ip')->nullable()->comment('IP');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
