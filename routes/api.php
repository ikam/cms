<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth相关的
Route::group([
    'prefix' => 'auth',
    'namespace'=>'Api',
], function () {
    Route::post('login', 'UserController@login');
    Route::post('register', 'UserController@register');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('index', 'UserController@index');
        Route::post('store', 'UserController@store');
        Route::get('{disk}/token', 'UserController@uploadToken');
        Route::get('logout', 'UserController@logout');
    });
});
Route::resource('devices', 'Api\DeviceController', ['only' => ['index']]);
