<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web 路由
|--------------------------------------------------------------------------
|
| 您可以在此处为您的应用程序注册web路由。 这些
| 路由由RouteServiceProvider加载到
| 包含“ Web”中间件组的组中。
|
*/

// 博客相关路由
Route::group([
    //'prefix' => '',
    'namespace'=>'\App\Http\Controllers',
], function () {
    Route::get('/', 'WebController@home');//首页
    Route::get('/test', function () {
        $str = '
        ![好图](https://b3logfile.com/bing/20181203.jpg?imageView2/1/w/768/h/432/interlace/1/q/100)
        ### 参考一下定义
        ![啊破图](https://b3logfile.com/bing/20181203.jpg?imageView2/1/w/768/h/432/interlace/1/q/100)
        ### 参考一下定义
        ';
        $partnner = '!\[(.*)\]\((.+)\)!';
        preg_match_all($partnner, $str, $match);
        dd($match);
        echo 111;
    });

    Route::group([
        'middleware' => 'auth:web'
    ], function() {
        Route::resource('arts', 'WebController');
        /*
        Route::get('/users', 'UsersController@index')->name('users.index');
        Route::get('/users/{user}', 'UsersController@show')->name('users.show');
        Route::get('/users/create', 'UsersController@create')->name('users.create');
        Route::post('/users', 'UsersController@store')->name('users.store');
        Route::get('/users/{user}/edit', 'UsersController@edit')->name('users.edit');
        Route::patch('/users/{user}', 'UsersController@update')->name('users.update');
        Route::delete('/users/{user}', 'UsersController@destroy')->name('users.destroy');
         */
    });
});

// 测试路由
Route::get('/welcome', function () {
    return view('welcome');
});

// Auth-web相关的
Auth::routes();
Route::namespace('Auth')->prefix('oauth')->group(function () {
    // 登录 third:qq weixin weibo github
    Route::get('{third}', 'LoginController@oauth');
    // 回调
    Route::get('{third}/callback', 'LoginController@callback');
});

//如果需要登录方可访问的页面
Route::group([
    'middleware' => 'auth:web'
], function() {
    Route::get('/passport', function () {
        return view('passport');
    });
});

